package Launch;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Client extends Application {

    private static Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setOnCloseRequest(event -> {
            Platform.exit();});
        Client.stage = stage;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("..//FXML/client.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root);
        stage.setResizable(false);
        stage.setHeight(280);
        stage.setWidth(280);
        stage.setScene(scene);
        stage.show();
    }

    public static Stage getPrimaryStage() {
        return stage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
