package Controllers;

import Classes.Listener;
import Launch.Client;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ClientController implements Initializable {

    @FXML
    public TextField clientLoginTxtfleld;
    @FXML
    public TextField clientPassTxtfleld;
    @FXML
    public TextField clientSocketPort;
    @FXML
    public TextField clientSocketIP;
    @FXML
    public Button clientConnectBtn;
    @FXML
    public CheckBox clientSocketCB;
    @FXML
    public Label clientSocketLabel;
    @FXML
    public VBox clientvbox;

    private double xOffset;
    private double yOffset;
    private Scene scene;

    public static ChatController controller;
    public static ClientController instance;

    public ClientController() {
        instance = this;
    }

    public static ClientController getInstance() {
        return instance;
    }

    public void clientConnectBtnAction(ActionEvent event) throws IOException {

        String hostname = clientSocketIP.getText();
        int port = Integer.parseInt(clientSocketPort.getText());

        String username = clientLoginTxtfleld.getText();
        String password = clientPassTxtfleld.getText();

        FXMLLoader fmxlLoader = new FXMLLoader(getClass().getResource("..//FXML/chat.fxml"));
        Parent window =  fmxlLoader.load();
        controller = fmxlLoader.getController();
        Listener listener = new Listener(hostname, port, username, controller);
        Thread x = new Thread(listener);
        x.start();
        this.scene = new Scene(window);
    }

    public void showScene() throws IOException {
        Platform.runLater(() -> {
            Stage stage = Client.getPrimaryStage();
            stage.setResizable(true);
            stage.setMinWidth(500);
            stage.setMinHeight(500);
            stage.setWidth(500);
            stage.setHeight(500);
            stage.setScene(scene);
            stage.centerOnScreen();
            controller.setUsernameLabel(controller.getUsernameLabel()+clientLoginTxtfleld.getText());
        });
    }

    public void showErrorDialog(String message) {
        Platform.runLater(()-> {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setHeaderText(message);
            alert.setContentText("Please check for firewall issues and check if the server is running.");
            alert.showAndWait();
        });

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        clientSocketCB.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                clientSocketIP.setDisable(false);
                clientSocketPort.setDisable(false);
                clientSocketLabel.setDisable(false);
            } else {
                clientSocketIP.setDisable(true);
                clientSocketPort.setDisable(true);
                clientSocketLabel.setDisable(true);
            }
        });

        clientvbox.setOnMousePressed(event -> {
            xOffset = Client.getPrimaryStage().getX() - event.getScreenX();
            yOffset = Client.getPrimaryStage().getY() - event.getScreenY();
            clientvbox.setCursor(Cursor.CLOSED_HAND);
        });

        clientvbox.setOnMouseDragged(event -> {
            Client.getPrimaryStage().setX(event.getScreenX() + xOffset);
            Client.getPrimaryStage().setY(event.getScreenY() + yOffset);

        });

        clientvbox.setOnMouseReleased(event -> clientvbox.setCursor(Cursor.DEFAULT));
    }
}
