package Controllers;


import Classes.CellRenderer;
import Classes.Listener;
import Classes.Message;
import Classes.User;
import Launch.Client;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class ChatController implements Initializable {

    @FXML
    public AnchorPane anchorPane;
    @FXML
    public Label usernameLabel;
    @FXML
    public Label onlineCountLabel;
    @FXML
    public TextArea messageArea;
    @FXML
    public ListView userList;
    @FXML
    public ListView chatPane;

    private final String connectString = "Connected as: ";
    private double xOffset;
    private double yOffset;

    @FXML
    public void sendButtonAction() throws IOException {
        String msg = messageArea.getText();
        if (!messageArea.getText().isEmpty()) {
            Listener.send(msg);
            messageArea.clear();
        }
    }

    public synchronized void addToChat(Message msg) {

        Task<HBox> othersMessages = new Task<HBox>() {
            @Override
            public HBox call() throws Exception {
                HBox x = new HBox();
                x.getChildren().add(new Label(msg.getName() + ": " + msg.getMsg()));
                setOnlineLabel(Integer.toString(msg.getOnlineCount()));
                return x;
            }
        };

        othersMessages.setOnSucceeded(event -> chatPane.getItems().add(othersMessages.getValue()));

        Task<HBox> yourMessages = new Task<HBox>() {
            @Override
            public HBox call() throws Exception {

                HBox x = new HBox();
                x.setAlignment(Pos.CENTER_RIGHT);
                x.getChildren().addAll(new Label(msg.getMsg()));
                setOnlineLabel(Integer.toString(msg.getOnlineCount()));
                return x;
            }
        };
        yourMessages.setOnSucceeded(event -> chatPane.getItems().add(yourMessages.getValue()));

        if ((connectString + msg.getName()).equals(usernameLabel.getText())) {
            Thread t2 = new Thread(yourMessages);
            t2.setDaemon(true);
            t2.start();
        } else {
            Thread t = new Thread(othersMessages);
            t.setDaemon(true);
            t.start();
        }

    }

    public void setUsernameLabel(String username) {
        this.usernameLabel.setText(username);
    }

    public String getUsernameLabel() {
        return usernameLabel.getText();
    }

    public void setOnlineLabel(String usercount) {
        Platform.runLater(() -> onlineCountLabel.setText("Online users: " + usercount));
    }

    public void setUserList(Message msg) {

        Platform.runLater(() -> {
            ObservableList<User> users = FXCollections.observableList(msg.getUsers());
            userList.setItems(users);
            userList.setCellFactory(new CellRenderer());
            setOnlineLabel(String.valueOf(msg.getUserlist().size()));


        });
    }


    public synchronized void notifications(Message msg) {

        Task<HBox> notification = new Task<HBox>() {
            @Override
            public HBox call() throws Exception {
                HBox x = new HBox();
                x.setAlignment(Pos.CENTER);
                x.getChildren().add(new Label(msg.getName() + msg.getMsg()));
                setOnlineLabel(Integer.toString(msg.getOnlineCount()));
                return x;
            }
        };

        notification.setOnSucceeded(event -> chatPane.getItems().add(notification.getValue()));
        setUserList(msg);
        new Thread(notification).start();
    }


    public synchronized void addAsServer(Message msg) {
        Task<HBox> task = new Task<HBox>() {
            @Override
            public HBox call() throws Exception {
                HBox x = new HBox();
                x.setAlignment(Pos.CENTER);
                x.getChildren().addAll(new Label(msg.getMsg()));
                return x;
            }
        };
        task.setOnSucceeded(event -> chatPane.getItems().add(task.getValue()));

        Thread t = new Thread(task);
        t.setDaemon(true);
        t.start();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
                /* Drag and Drop */
        anchorPane.setOnMousePressed(event -> {
            xOffset = Client.getPrimaryStage().getX() - event.getScreenX();
            yOffset = Client.getPrimaryStage().getY() - event.getScreenY();
            anchorPane.setCursor(Cursor.CLOSED_HAND);
        });

        anchorPane.setOnMouseDragged(event -> {
            Client.getPrimaryStage().setX(event.getScreenX() + xOffset);
            Client.getPrimaryStage().setY(event.getScreenY() + yOffset);

        });

        anchorPane.setOnMouseReleased(event -> anchorPane.setCursor(Cursor.DEFAULT));


        messageArea.addEventFilter(KeyEvent.KEY_PRESSED, ke -> {
            if (ke.getCode().equals(KeyCode.ENTER)) {
                try {
                    sendButtonAction();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ke.consume();
            }
        });

    }

    @FXML
    public void sendMethod(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            sendButtonAction();
        }
    }

    public void logoutScene() {
        Client.getPrimaryStage().setOnCloseRequest(event -> Platform.exit());
        Platform.runLater(() -> {
            FXMLLoader fmxlLoader = new FXMLLoader(getClass().getResource("..//FXML/client.fxml"));
            Parent window = null;
            try {
                window = fmxlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage stage = Client.getPrimaryStage();

            Scene scene = new Scene(window);
            stage.setResizable(false);

            stage.setHeight(280);
            stage.setWidth(280);

            stage.setMinHeight(280);
            stage.setMinWidth(280);

            stage.setScene(scene);
            stage.centerOnScreen();
        });
    }


}