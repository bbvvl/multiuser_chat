package Classes;

import Controllers.ChatController;
import Controllers.ClientController;
import Launch.Client;
import javafx.application.Platform;


import java.io.*;
import java.net.Socket;

import static Classes.MessageType.CONNECTED;
import static Classes.MessageType.DISCONNECTED;

public class Listener implements Runnable {

    private static final String HASCONNECTED = "has connected";

    private Socket socket;
    private String hostname;
    private int port;
    private static String username;
    private ChatController controller;
    private static ObjectOutputStream oos;
    private InputStream is;
    private ObjectInputStream input;
    private OutputStream outputStream;


    public Listener(String hostname, int port, String username, ChatController controller) {
        this.hostname = hostname;
        this.port = port;
        Listener.username = username;
        this.controller = controller;
    }

    public void run() {
        try {
            socket = new Socket(hostname, port);
            ClientController.getInstance().showScene();
            Client.getPrimaryStage().setOnCloseRequest(event -> {
                try {
                    disconnect();
                    socket.close();
                } catch (IOException e) {
                    System.out.println("This error occurs when the server is disabled");
                }

                Platform.exit();
            });
            outputStream = socket.getOutputStream();
            oos = new ObjectOutputStream(outputStream);
            is = socket.getInputStream();
            input = new ObjectInputStream(is);
        } catch (IOException e) {
            ClientController.getInstance().showErrorDialog("Could not connect to server");

            return;
        }

        try {
            connect();

            while (socket.isConnected()) {
                Message message;
                message = (Message) input.readObject();

                if (message != null) {

                    switch (message.getType()) {
                        case USER:
                            controller.addToChat(message);
                            break;
                        case NOTIFICATION:
                            controller.notifications(message);
                            break;
                        case SERVER:
                            controller.addAsServer(message);
                            break;
                        case CONNECTED:
                            controller.setUserList(message);
                            break;
                        case DISCONNECTED:
                            controller.notifications(message);
                            break;

                    }
                }
            }

            socket.close();

        } catch (IOException | ClassNotFoundException e) {
            try {
                socket.close();
            }catch (IOException e1){}

            controller.logoutScene();

        }

    }

    public static void send(String msg) throws IOException {
        Message createMessage = new Message();
        createMessage.setName(username);
        createMessage.setType(MessageType.USER);
        createMessage.setMsg(msg);
        oos.writeObject(createMessage);
        oos.flush();
    }


    private static void connect() throws IOException {
        Message createMessage = new Message();
        createMessage.setName(username);
        createMessage.setType(CONNECTED);
        createMessage.setMsg(HASCONNECTED);
        oos.writeObject(createMessage);
    }

    private static void disconnect() throws IOException {
        Message createMessage = new Message();

        createMessage.setName(username);
        createMessage.setType(DISCONNECTED);
        createMessage.setMsg(DISCONNECTED.toString());
        oos.writeObject(createMessage);

    }
}
