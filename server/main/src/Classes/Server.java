package Classes;

import Exceptions.DuplicateUsernameException;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Server implements Runnable {

    @FXML
    private TextArea area;
    private int portNumber;


    public Server(TextArea area, int port) throws IOException {
        this.portNumber = port;
        this.area = area;


    }

    public void run() {

        try {

            ServerSocket socket = new ServerSocket(portNumber);

            try {

                while (true) {
                    Thread client = new Handler(socket.accept(), area);
                    client.start();

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            socket.close();
        } catch (IOException e) {
            //  area.setText("WTF????");
        }
    }

    private static final HashMap<String, User> names = new HashMap<>();
    private static HashSet<ObjectOutputStream> writers = new HashSet<>();
    private static ArrayList<User> users = new ArrayList<>();

    private static class Handler extends Thread {
        private String name;
        private Socket socket;
        private User user;
        private TextArea area;

        Handler(Socket socket, TextArea area) throws IOException {
            this.socket = socket;
            this.area = area;
            setDaemon(true);
        }

        public void run() {
            area.appendText("\n" + "Attempting to connect a user...");

            try (
                    InputStream is = socket.getInputStream();
                    ObjectInputStream input = new ObjectInputStream(is);
                    OutputStream os = socket.getOutputStream();
                    ObjectOutputStream output = new ObjectOutputStream(os)
            ) {


                Message firstMessage = (Message) input.readObject();
                checkDuplicateUsername(firstMessage);
                writers.add(output);
                addToList();
                sendNotification(firstMessage);

                while (socket.isConnected()) {
                    Message inputmsg = (Message) input.readObject();
                    if (inputmsg != null) {
                        switch (inputmsg.getType()) {
                            case USER:
                                area.appendText("\n" + inputmsg.getName() + ": " + inputmsg.getMsg());
                                write(inputmsg);
                                break;
                            case CONNECTED:
                                addToList();
                                break;
                            case DISCONNECTED:
                                closeConnections();
                                break;

                        }
                    }
                }
            } catch (IOException | DuplicateUsernameException | ClassNotFoundException e) {
                //e.printStackTrace();
            }
        }

        private synchronized void checkDuplicateUsername(Message firstMessage) throws DuplicateUsernameException, IOException {
            area.appendText("\n" + firstMessage.getName() + " is trying to connect");

            if (!names.containsKey(firstMessage.getName())) {
                this.name = firstMessage.getName();
                user = new User();
                user.setName(firstMessage.getName());

                users.add(user);
                names.put(name, user);

                area.appendText("\n" + name + " has been added to the list");
            } else {
                area.appendText("\n" + firstMessage.getName() + " is already connected");
                throw new DuplicateUsernameException(firstMessage.getName() + " is already connected");
            }
        }

        private Message sendNotification(Message firstMessage) throws IOException {
            Message msg = new Message();
            msg.setMsg(" has connected the chat.");
            msg.setType(MessageType.NOTIFICATION);
            msg.setName(firstMessage.getName());

            write(msg);
            return msg;
        }


        private void write(Message msg) throws IOException {

            for (ObjectOutputStream writer : writers) {
                msg.setUserlist(names);
                msg.setUsers(users);
                msg.setOnlineCount(names.size());
                try {
                    writer.writeObject(msg);
                    writer.reset();
                } catch (Exception ex) {
                    //closeConnections();
                    //System.out.println("WTF");
                }
            }
        }

        private Message addToList() throws IOException {
            Message msg = new Message();
            msg.setMsg("Welcome, You have now joined the server! Enjoy chatting!");
            msg.setType(MessageType.CONNECTED);
            msg.setName("SERVER");
            write(msg);
            return msg;
        }


        private synchronized void closeConnections() {
            area.appendText("\n" + "closeConnections() method Enter");

            if (name != null) {

                users.remove(names.get(name));
                names.remove(name);
                area.appendText("\n" + "User: " + name + " has been removed!");

            }

            try {
                removeFromList();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("1");
            }

            area.appendText("\n" + "closeConnections() method Exit");
        }

        private Message removeFromList() throws IOException {
            area.appendText("\n" + "removeFromList() method Enter");
            Message msg = new Message();
            msg.setMsg(" has left from the chat.");
            msg.setType(MessageType.DISCONNECTED);
            msg.setName(name);
            msg.setUserlist(names);
            write(msg);
            area.appendText("\n" + "removeFromList() method Exit");
            return msg;
        }


    }


}
