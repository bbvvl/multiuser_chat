package Controllers;

import Classes.Server;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.IOException;


public class ServerController {

    @FXML
    private Button connectBtn;
    @FXML
    private Button disconnectBtn;
    @FXML
    private TextArea serverTxtArea;
    @FXML
    private TextField serverPort;

    private Thread serverThread;
    private Server server;

    @FXML
    public void connectBtnAction(ActionEvent event) throws IOException {


        try {

            server = new Server(serverTxtArea,
                    Integer.parseInt(serverPort.getText()));
            serverThread = new Thread(server);

            serverTxtArea.setText("Server has been started!");
            serverTxtArea.appendText("\n");
            serverTxtArea.appendText("Waiting for a client...");


            serverThread.setDaemon(true);

            serverThread.start();


        } catch (IllegalArgumentException e) {
            serverTxtArea.setText("Please enter the correct port value");
        } ;

        disconnectBtn.setDisable(false);

    }

    @FXML
    public void disconnectBtnAction(ActionEvent event) {


        serverThread.interrupt();

        serverTxtArea.setText("Disconnected from server!");

        disconnectBtn.setDisable(true);
    }

}


